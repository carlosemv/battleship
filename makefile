# commands
CXX = gcc
DEL = rm

# options
DEBUG = -g
WARNING = -Wall
STD = -std=c99
OPTIONSCXX = $(WARNING) $(STD)

# directories
DECLRDIR = -I . -I ./include
SRCDIR = ./src/
BINDIR = ./bin/


### executable target
main: main.o AI.o player.o battleship.o
	$(CXX) $(BINDIR)main.o $(BINDIR)AI.o $(BINDIR)player.o $(BINDIR)battleship.o -o main $(OPTIONSCXX)

### tests (main)
main.o: $(SRCDIR)main.c
	$(CXX) $(SRCDIR)main.c -c -o $(BINDIR)main.o $(OPTIONSCXX) $(DECLRDIR)

### ai class
AI.o: $(SRCDIR)AI.c
	$(CXX) $(SRCDIR)AI.c -c -o $(BINDIR)AI.o $(OPTIONSCXX) $(DECLRDIR)

### player class
player.o: $(SRCDIR)player.c
	$(CXX) $(SRCDIR)player.c -c -o $(BINDIR)player.o $(OPTIONSCXX) $(DECLRDIR)

### battleship class
battleship.o: $(SRCDIR)battleship.c
	$(CXX) $(SRCDIR)battleship.c -c -o $(BINDIR)battleship.o $(OPTIONSCXX) $(DECLRDIR)


### clear objects & executable
clean:
	$(DEL) ./main
	$(DEL) $(BINDIR)*.o