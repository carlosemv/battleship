#ifndef __BOARD_H__
#define __BOARD_H__

#include <stdlib.h> // Rand
#include <stdio.h> // printBoard

#define BOARD_SIZE 9

typedef enum ShipType {
	NONE = 0,
	SUBMARINE = 1,
	DESTROYER = 2,
	CRUISER = 3,
	BATTLESHIP = 4,
	WRECK = 5,
	MISS = 6

} ShipType;

static int maxShipSize = BATTLESHIP;
static char* shipStrings[] = {" . ", " 1 ", " 2 ", " 3 ", " 4 ", " * ", " - "};
static ShipType allShips[] = {BATTLESHIP, CRUISER, CRUISER, DESTROYER,
		DESTROYER, DESTROYER, SUBMARINE, SUBMARINE, SUBMARINE, SUBMARINE};

typedef struct
{
	ShipType ship;
	char* string;
} BoardCell;


BoardCell playerBoard[BOARD_SIZE][BOARD_SIZE];
BoardCell playerHitboard[BOARD_SIZE][BOARD_SIZE];

BoardCell AIBoard[BOARD_SIZE][BOARD_SIZE];
BoardCell AIHitboard[BOARD_SIZE][BOARD_SIZE];

static inline void initBoard (BoardCell board[BOARD_SIZE][BOARD_SIZE])
{
	int i, j;
	for (i = 0; i < BOARD_SIZE; ++i) {
		for (j = 0; j < BOARD_SIZE; ++j) {
			board[i][j].string = shipStrings[NONE];
			board[i][j].ship = NONE;
		}
	}
}

static inline void printBoard (BoardCell board[BOARD_SIZE][BOARD_SIZE])
{
	int i, j;
	char l = 'A';

	printf(" ");
	for (i = 1; i <= BOARD_SIZE; ++i) {
		i < 10 ? printf(" %i ", i):printf(" %i", i);
	}
	printf("\n");

	for (i = 0; i < BOARD_SIZE; ++i) {
		printf("%c", l+i);
		for (j = 0; j < BOARD_SIZE; ++j) {
			printf("%s", board[i][j].string);
		}
		printf("\n");
	}
}

#endif
