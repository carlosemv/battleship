#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <stdlib.h> // clearScreen
#include <ctype.h> // toupper (getPlayerShot)

void setPlayerBoard();

void showBoards();

void getPlayerShot(int* x, int* y);

int checkShot(int x, int y);

void clearScreen();

#endif
