#ifndef __AI_H__
#define __AI_H__

#include <stdbool.h> // Bool
#include "board.h" // ShipType

typedef enum Direction {
	UP,
	RIGHT,
	DOWN,
	LEFT
} Direction;

// Required
void setAIBoard();

void directionFlag (int* i, int* j, Direction d);

bool checkPath(int xPos, int yPos, Direction d, ShipType ship);

void setShip (int xPos, int yPos, Direction d, ShipType ship);

unsigned int rand_interval(unsigned int min, unsigned int max);

//Required
int checkHit(int x, int y);

//Required
void shoot(int* x, int* y);

//Required
char* getName();


#endif
