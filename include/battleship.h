#ifndef __BATTLESHIP_H__
#define __BATTLESHIP_H__

#include <stdio.h> // (gameLoop, battleship)
#include <time.h> // time (init)
#include <stdbool.h> // Bool

#include "player.h"
#include "AI.h"

#define MAX_TURNS 1000

typedef struct
{
    bool hit;
    int x;
    int y;
} Shot;

void battleship();

void init();

int gameLoop();

#endif
