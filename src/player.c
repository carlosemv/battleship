#include "board.h"
#include "player.h"

void setPlayerBoard()
{
	initBoard(playerBoard);
	initBoard(playerHitboard);
}

void showBoards()
{
	printf("\n\tOpponent's board\n");
	printBoard(playerHitboard);
	printf("\n\tMy board\n");
	printBoard(playerBoard);
}

void getPlayerShot(int* x, int* y)
{
	char t[4];

	do {
	printf("Please type target (e.g. A3): ");
	scanf("%3s", t);
	} while (toupper(t[0]) < 'A' || toupper(t[0]) >= ('A' + BOARD_SIZE) ||
	t[1] < '1' || t[1] >= ('1' + BOARD_SIZE));

	*x = (toupper(t[0]) % 'A');
	*y = (t[1] % '1');
}

int checkShot(int x, int y)
{
	if (x < 0 || x >= BOARD_SIZE || y < 0 || y >= BOARD_SIZE) {
		return -1;
	}
	if (playerBoard[x][y].ship > maxShipSize) {
		return 0;
	}
	return (playerBoard[x][y].ship);
}

void clearScreen() 
{
    #if defined WIN32
    system("cls");
    #else
    system("clear");
    #endif
}