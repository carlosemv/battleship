#include "battleship.h"

void battleship()
{
	init();

	int winner = gameLoop();

    if (winner == 1) {
        printf("\t\tCongratulations! You Won!!\n");
    }
    else {
        printf("\t\tYou Lost! Try Again?\n");
    }
}

void init()
{
	srand( (unsigned)time(NULL) );
	setAIBoard();
    setPlayerBoard();
}

int gameLoop()
{
    int playerHits = 0;
    int AIHits = 0;
    int maxHits = 0;

    int i;
    int sz = sizeof(allShips) / sizeof(ShipType);
    for (i = 0; i < sz; ++i) {
        maxHits += allShips[i];
    }

    int x, y;
    int turn = 0;
    Shot lastPlayerShot, lastAIShot;
    lastAIShot.hit = false;
    lastPlayerShot.hit = false;

    while(turn < MAX_TURNS) {

        clearScreen();
        if (turn > 0) {
            if (lastPlayerShot.hit) {
                printf("You hit!\n");
            }
            else {
                printf("You missed.\n");
            }
            if (lastAIShot.hit) {
                printf("You were hit.\n");
            }
            else {
                printf("They missed;\n");
            }
        }
        showBoards();

        getPlayerShot(&x, &y);
        if (checkHit(x, y) > 0)
        {
            // change ai board
            playerBoard[x][y].ship = WRECK;
            playerBoard[x][y].string = shipStrings[WRECK];

            // change the player's tracking of the ai's board
            playerHitboard[x][y].ship = WRECK;
            playerHitboard[x][y].string = shipStrings[WRECK];

            // update last player shot to a hit
            lastPlayerShot.hit = true;
            lastPlayerShot.x = x;
            lastPlayerShot.y = y;
            playerHits++;
        }
        else
        {
            // update last player shot to a miss
            lastPlayerShot.hit = false;

            if (playerHitboard[x][y].ship == NONE) {
                // change the player's tracking of the ai's board
                playerHitboard[x][y].ship = MISS;
                playerHitboard[x][y].string = shipStrings[MISS];
            }
        }

        if (playerHits >= maxHits) {
            return 1;
        }

        shoot(&x, &y);
        if (checkShot(x, y) > 0)
        {
            // change player board
            playerBoard[x][y].ship = WRECK;
            playerBoard[x][y].string = shipStrings[WRECK];

            // change the ai's tracking of the player's board
            AIHitboard[x][y].ship = WRECK;
            AIHitboard[x][y].string = shipStrings[WRECK];

            // Update last ai shot to a hit
            lastAIShot.hit = true;
            lastAIShot.x = x;
            lastAIShot.y = y;
            AIHits++;
        }
        else
        {
            // update last ai shot to a miss
            lastAIShot.hit = false;

            if (AIHitboard[x][y].ship == NONE) {
                // change the ai's tracking of the player's board
                AIHitboard[x][y].ship = MISS;
                AIHitboard[x][y].string = shipStrings[MISS];
            }
        }

        if (AIHits >= maxHits) {
            return 0;
        }

        turn++;
    }

    return -1;

}