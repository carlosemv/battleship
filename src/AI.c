#include "AI.h"
#include "board.h"

void directionFlag (int* i, int* j, Direction d) {
	if (d == UP) {
		*i = -1;
		*j = 0;
	}
	else if (d == RIGHT) {
		*i = 0;
		*j = 1;
	}
	else if (d == DOWN) {
		*i = 1;
		*j = 0;
	}
	else if (d == LEFT) {
		*i = 0;
		*j = -1;
	}
}

void setAIBoard()
{
	initBoard(AIBoard);
	initBoard(AIHitboard);

	int sz = sizeof(allShips) / sizeof(ShipType);

	int i = 0;
	unsigned int xPos, yPos;

	while (i < sz)
	{
		do {
			xPos = rand_interval(0, (BOARD_SIZE-1));
			yPos = rand_interval(0, (BOARD_SIZE-1));
		} while (AIBoard[xPos][yPos].ship != NONE);

		if (checkPath(xPos, yPos, UP, allShips[i])) {
			setShip(xPos, yPos, UP, allShips[i]);
			++i;
		}
		else if (checkPath(xPos, yPos, RIGHT, allShips[i])) {
			setShip(xPos, yPos, RIGHT, allShips[i]);
			++i;
		}
		else if (checkPath(xPos, yPos, DOWN, allShips[i])) {
			setShip(xPos, yPos, DOWN, allShips[i]);
			++i;
		}
		else if (checkPath(xPos, yPos, LEFT, allShips[i])) {
			setShip(xPos, yPos, LEFT, allShips[i]);
			++i;
		}
		
	}

	printBoard(AIHitboard);
	printBoard(AIBoard);
}

bool checkPath(int xPos, int yPos, Direction d, ShipType ship)
{
	int i, j;
	directionFlag(&i, &j, d);

	int k, x, y;

	for (k = 0; k < ship; ++k) { 
		x = xPos + (k*i);
		y = yPos + (k*j);

		if (x < 0 || x >= BOARD_SIZE || y < 0 || y >= BOARD_SIZE) {
			return false;
		}
		else if (AIBoard[x][y].ship != NONE) {
			return false;
		}

	}

	return true;
}

void setShip (int xPos, int yPos, Direction d, ShipType ship)
{
	int i, j;
	directionFlag(&i, &j, d);

	int k, x, y;

	for (k = 0; k < ship; ++k) { 
		x = xPos + (k*i);
		y = yPos + (k*j);

		AIBoard[x][y].ship = ship;
		AIBoard[x][y].string = shipStrings[ship];
	}
}


int checkHit(int x, int y)
{
	if (x < 0 || x >= BOARD_SIZE || y < 0 || y >= BOARD_SIZE) {
		return -1;
	}
	if (AIBoard[x][y].ship > maxShipSize) {
		return 0;
	}
	return (AIBoard[x][y].ship);
}

void shoot(int* x, int* y)
{
	unsigned xPos, yPos;
	do {
		xPos = rand_interval(0, (BOARD_SIZE-1));
		yPos = rand_interval(0, (BOARD_SIZE-1));
	} while (AIHitboard[xPos][yPos].ship != NONE);

	*x = xPos;
	*y = yPos;
}

unsigned int rand_interval(unsigned int min, unsigned int max)
{
    int r;
    const unsigned int range = 1 + max - min;
    const unsigned int buckets = RAND_MAX / range;
    const unsigned int limit = buckets * range;

    do {
        r = rand();
    } while (r >= limit);

    return min + (r / buckets);
}

char* getName()
{
	return "POTEMKIN";
}
